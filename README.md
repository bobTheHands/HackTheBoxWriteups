# HackTheBox Writeups

I have been trying to give back to the community by drafting writeup reports for the machine I've completed on Hack the Box, which is a website for practising ethical hacking.

## Beginner-Friendly All The Way
I pitch every report for a 'beginner', regardless of the difficulty of the machine. I've made this choice as I consider writeups to be a great learning resource for people trying to get started in the area. I am always available for for a chat when it comes to cyber, and especially ethical hacking for beginners. 

If you're a beginner looking for tips on how to take effective notes, please see my file on 'HTB Notetaking Tips'

## Retired Machines
Retired machines are free to peruse in their own folder above, with no password. Download the PDF, as it renders slowly and weirdly on the Github viewer. 

## Active Machines
Active machines are downloadable PDFs, locked with passwords. Click on the PDF you wamt and download it to your computer. 
With each active box, I state the required password you will need to unlock it when prompted after you open it on your computer, so please keep an eye out for this. 

### Blackfield
Password is Admin's hash, starting with 184 ending with 9ee

### Blunder
Password is the root hash beginning with $6$ and ending with 18228

### Buff
Password is Admin Hash

### Cache
Password is root hash, starts with bW ends with kb/

### Fuse
Password protected with Administrator's hash beginning with 370 and ending with 14e

### Tabby
Password is root's shadow hash, include the beginning $6$ and end with 0H/
